/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Page1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void testPhone1() {
        String phone = "+7-958-859-39-89";
        assertTrue(Main.checkPhone(phone), "testPhone1");
    }

    @Test void testPhone2() {
        String phone = "+43-432-432-43-43";
        assertTrue(Main.checkPhone(phone), "testPhone2");
    }

    @Test void testPhone3() {
        String phone = "44-432-432-43-43";
        assertTrue(Main.checkPhone(phone), "testPhone3");
    }

    @Test void testPhone4() {
        String phone = "4-432-432-43-43";
        assertTrue(Main.checkPhone(phone), "testPhone4");
    }

    @Test void testPhone5() {
        String phone = "+5-(432)-432-43-43";
        assertTrue(Main.checkPhone(phone), "testPhone5");
    }

    @Test void testPhone6() {
        String phone = "+54-(432)-432-43-43";
        assertTrue(Main.checkPhone(phone), "testPhone6");
    }

    @Test void testPhone7() {
        String phone = "+5 432 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone7");
    }

    @Test void testPhone8() {
        String phone = "+55 432 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone8");
    }

    @Test void testPhone9() {
        String phone = "55 432 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone9");
    }

    @Test void testPhone10() {
        String phone = "+5 432 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone10");
    }

    @Test void testPhone11() {
        String phone = "5 432 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone11");
    }

    @Test void testPhone12() {
        String phone = "+55 (432) 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone12");
    }

    @Test void testPhone13() {
        String phone = "45 (432) 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone13");
    }

    @Test void testPhone14() {
        String phone = "+5 (432) 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone14");
    }

    @Test void testPhone15() {
        String phone = "5 (432) 432 43 43";
        assertTrue(Main.checkPhone(phone), "testPhone15");
    }

    @Test void testPhone16() {
        String phone = "554324324343";
        assertTrue(Main.checkPhone(phone), "testPhone16");
    }

    @Test void testPhone17() {
        String phone = "+554324324343";
        assertTrue(Main.checkPhone(phone), "testPhone17");
    }
}