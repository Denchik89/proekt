package Page2;

/**
 * Класс Client
 * использует интерфейс Runnable
 * @author Denis
 * @version 1.0
 */
public class Client implements Runnable {

    /** @value имя клиента */
    String name;

    /** @value комната ожидания */
    private final Room room;

    /** @value рабочее место парикмахера */
    private final Chair chair;

    /** конструктор Client
     * @param chair - рабочее место парикмахера
     * @param room - комната ожидания
     */
    Client(Room room, Chair chair) {
        this.chair = chair;
        this.room = room;
    }

    /**
     * метод setName
     * @return возвращает имя клиента
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * метод getNameTour
     * отправляет клиента на стрижку
     */
    public synchronized void goToChair() {
        room.leave();
        try {
            chair.occupy();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /** метод leaveChair
     * отправляет клиента домой
     */
    public synchronized void leaveChair() {
        chair.leave();
    }


    /** методо run
     * метод потока
     */
    public void run() {
        try {
            if (room.takePlace()) {
                System.out.println(this.name + " находится в комнате");
                chair.takePlaceInQueue();
                System.out.println(this.name + " ннаходится на стуле");
                goToChair();
                System.out.println(this.name + " парикмахер постриг и он уходит");
                leaveChair();
            } else {
                System.out.println("свободных мест нет " + this.name + " уходит");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
