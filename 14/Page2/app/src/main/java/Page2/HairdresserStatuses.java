package Page2;

/**
 * enum класс HairdresserStatuses
 * @author Denis
 * @version 1.0
 */
public enum HairdresserStatuses {
    WORKING, SLEEPING, FREE
}
