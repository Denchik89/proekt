package Page2;

/**
 * Класс Customers
 * использует интерфейс Runnable
 * @author Denis
 * @version 1.0
 */
public class Customers implements Runnable {

    /** @value комната в которую приходят клиенты */
    Room room;

    /** @value рабочее место парикмахера, где он стрижет */
    Chair chair;

    /** @value начало диапозон рандомного засыпания потока */
    static final int LATENCY_START = 4000;

    /** @value конец диапозона рандомного засыпания потока */
    static final int LATENCY_FINISH = 7000;


    /** конструктор Customers
     * @param room - комната клиентов
     * @param chair - рабочее место парикмахера
     */
    Customers(Room room, Chair chair) {
        this.room = room;
        this.chair = chair;
    }

    /** методо run
     * метод потока
     */
    public void run() {
        int i = 0;
        while (true) {
            try {
                Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
                i++;
                Client client = new Client(room, chair);
                client.setName("Клиент " + i);
                new Thread(client).start();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}