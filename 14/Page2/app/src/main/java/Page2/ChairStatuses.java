package Page2;

/**
 * enum класс ChairStatuses
 * @author Denis
 * @version 1.0
 */
public enum ChairStatuses {
    OLD_CLIENT, NEW_CLIENT, FREE, QUEUED
}