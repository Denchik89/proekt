package Page2;

import java.util.concurrent.Semaphore;

/**
 * Класс Room
 * @author Denis
 * @version 1.0
 */
public class Room {

    public static final int MAX_VALUE_ROOM = 10;
    /** @value количество мест в комнате */
    public Semaphore placeRoom;

    /** конструктор Room
     * @param capacity - количество мест в комнате
     */
    public Room(int capacity) {
        this.placeRoom = new Semaphore(capacity, true);
    }

    /**
     * метод takePlace
     * позволяет клиенту занять место в комнате, если там есть свободные места
     * @return - boolean есть ли в комнате свободные места
     * @throws InterruptedException
     */
    public boolean takePlace() throws InterruptedException {
        return placeRoom.tryAcquire();
    }

    /** метод isEmpty
     * проверка комнаты на пустоту
     * @return - boolean пустая ли комната
     */
    public synchronized boolean isEmpty() {
        return !(this.placeRoom.availablePermits() < MAX_VALUE_ROOM);
    }

    /** метод leave
     * освобождение комнаты
     */
    public synchronized void leave() {
        this.placeRoom.release();
    }
}
