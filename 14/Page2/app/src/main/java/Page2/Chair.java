package Page2;

import java.util.concurrent.Semaphore;

/**
 * Класс Chair
 * @author Denis
 * @version 1.0
 */
public class Chair {

    /** @value парикмахер работающий на этом стуле */
    public Hairdresser hairdresser;

    /** @value место на стуле */
    Semaphore place = new Semaphore(1, true);

    /** @value статус стула */
    public ChairStatuses status = ChairStatuses.FREE;

    /** метод setHairdresser
     * назначение парикмахера
     * @param hairdresser - парикмахер которого нужно назначить
     */
    public void setHairdresser(Hairdresser hairdresser) {
        this.hairdresser = hairdresser;
    }

    /** метод occupy
     * используется для обозначения, что клиент занял стул
     * @throws InterruptedException
     */
    public synchronized void occupy() throws InterruptedException {
        this.status = ChairStatuses.NEW_CLIENT;
        notifyAll();
        wait();
    }

    /** метод leave
     * используется для того, что клиент освободил стул
     */
    public synchronized void leave() {
        if (this.place.hasQueuedThreads()){
            this.status = ChairStatuses.QUEUED;
        } else {
            this.status = ChairStatuses.FREE;
        }
        this.place.release();
        notifyAll();
    }

    /** метод sleepChair
     * используется для парикмахера, чтобы спал на стуле
     */
    public synchronized void sleepChair() {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
    }

    /** метод takePlaceInQueue
     * для занятия очереди на стул
     * @throws InterruptedException
     */
    public void takePlaceInQueue() throws InterruptedException {
        this.place.acquire();
    }

    /** метод ring
     * для оповещения клиента на стуле
     */
    public synchronized void ring() {
        notifyAll();
    }
}