package Page2;

/**
 * Класс Hairdresser
 * использует интерфейс Runnable
 * @author Denis
 * @version 1.0
 */
public class Hairdresser implements Runnable {

    /** конструктор Hairdresser
     * @param room - комната клиентов
     * @param chair - рабочее место парикмахера
     */
    public Hairdresser(Room room, Chair chair) {
        this.room = room;
        this.chair = chair;
    }

    /** @value начало диапозон рандомного засыпания потока */
    static final int LATENCY_START = 4000;

    /** @value конец диапозона рандомного засыпания потока */
    static final int LATENCY_FINISH = 7000;

    /** @value статус парикмахера */
    public HairdresserStatuses status;

    /** @value комната ожидания */
    public Room room;

    /** @value рабочее место парикмахера */
    public Chair chair;

    /**
     * метод goToSleep
     * отправляет парикмахера спать
     */
    public synchronized void goToSleep() throws InterruptedException {
        this.status = HairdresserStatuses.SLEEPING;
        System.out.println("парикмахер спит");
        this.chair.sleepChair();
    }

    /**
     * метод goToWork
     * отправляет парикмахера работать
     */
    public synchronized void goToWork() {
        try {
            System.out.println("парикмахер начинает свою работу");
            this.status = HairdresserStatuses.WORKING;
            Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
            this.status = HairdresserStatuses.FREE;
            System.out.println("парикмахер закончил свою работу");
            chair.ring();
            chair.status = ChairStatuses.OLD_CLIENT;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    /** методо run
     * метод потока
     */
    public void run() {
        while (true) {
            try {
                if (chair.status == ChairStatuses.NEW_CLIENT) {
                    System.out.println("парикмахер приступает к своей работе");
                    goToWork();
                }
                if (chair.status == ChairStatuses.FREE) {
                    if (room.isEmpty()) {
                        goToSleep();
                    }
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}