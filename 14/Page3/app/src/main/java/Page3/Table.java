package Page3;

import java.util.ArrayList;

/**
 * Класс Table
 * @author Denis
 * @version 1.0
 */
public class Table {

    /** @value спички */
    private boolean hasMatches;

    /** @value табак */
    private boolean hasTobacco;

    /** @value бумага */
    private boolean hasPaper;

    /**
     * метод hasMatches
     * проверяет есть ли спички
     * @return boolean
     */
    public boolean hasMatches() {
        return hasMatches;
    }
    /**
     * метод hasTobacco
     * проверяет есть ли табак
     * @return boolean
     */
    public boolean hasTobacco() {
        return hasTobacco;
    }

    /**
     * метод hasPaper
     * проверяет есть ли бумага
     * @return boolean
     */
    public boolean hasPaper() {
        return hasPaper;
    }

    /**
     * метод getMatches
     * выдает спички, если они есть
     * @return ResourseStatus.MATCHES
     */
    public synchronized ResourceType getMatches() {
        if (hasMatches()) {
            hasMatches = false;
            System.out.println("Взяли спички");
            return ResourceType.MATCHES;
        } else {
            System.out.println("Спичек нет");
            return null;
        }
    }

    /**
     * метод getTobacco
     * выдает табак, если он есть
     * @return ResourseStatus.TOBACCO
     */
    public synchronized ResourceType getTobacco() {
        if (hasTobacco()) {
            hasTobacco = false;
            System.out.println("Взяли табак");
            return ResourceType.TOBACCO;
        } else {
            System.out.println("Табака нет");
            return null;
        }
    }

    /**
     * метод getPaper
     * выдает бумагу, если она есть
     * @return ResourseStatus.PAPER
     */
    public synchronized ResourceType getPaper() {
        if (hasPaper()) {
            hasPaper = false;
            System.out.println("Взяли бумагу");
            return ResourceType.PAPER;
        } else {
            System.out.println("Бумаги нет");
            return null;
        }
    }

    /**
     * метод putResourceToTheTable
     * кладед ресурсы на стол
     * @param resources - лист ресусров
     */
    public void putResourceToTheTable(ArrayList<ResourceType> resources) {
        for (ResourceType resourceType : resources) {
            switch (resourceType) {
                case MATCHES:
                    if (!hasMatches) {
                        System.out.println("На стол положили спички");
                        hasMatches = true;
                    }
                    break;
                case TOBACCO:
                    if (!hasTobacco) {
                        System.out.println("На стол положили табак");
                        hasTobacco = true;
                    }
                    break;
                case PAPER:
                    if (!hasPaper) {
                        System.out.println("На стол положили бумагу");
                        hasPaper = true;
                    }
                    break;
            }
        }
    }

}