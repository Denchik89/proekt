package Page3;

import java.util.ArrayList;

/**
 * Класс Barman
 * использует интерфейс Runnable
 * @author Denis
 * @version 1.0
 */
public class Barman implements Runnable {

    /** @value начало диапозон рандомного засыпания потока */
    private static final int LATENCY_START = 4000;

    /** @value конец диапозона рандомного засыпания потока */
    private static final int LATENCY_FINISH = 7000;
    
    /** @value количество ждущих курильщиков */
    public static final int NUMBER_OF_SMOKERS = 2;

    /** @value стол на котором обслуживает бармен */
    private Table table;

    /** @value курильщики */
    private Smoker[] smokers;

    /** @value список курильщиков */
    private ArrayList<Smoker> waitingSmokers = new ArrayList<>();

    /**
     * конструктор Barman
     * @param table - стол бармена
     * @param smokers - список курильщиков
     */
    Barman(Table table, Smoker[] smokers) {
        this.table = table;
        this.smokers = smokers;
    }

    /**
     * метод smokersUpdate
     * обновляет всех ожидающих курильщиков
     */
    private void smokersUpdate() {
        waitingSmokers.clear();
        for (Smoker smoker : smokers) {
            if (smoker.getStatus() == SmokerStatus.WAITING) {
                waitingSmokers.add(smoker);
            }
        }
    }

    /**
     * метод который запускает поток
     */
    public void run() {
        while (true) {
            try {
                ArrayList<ResourceType> takenResources = new ArrayList<>();
                Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
                smokersUpdate();
                if (waitingSmokers.size() >= NUMBER_OF_SMOKERS) {
                    System.out.println("Бармен кладет ресурсы");
                    for (int i = 0; i < NUMBER_OF_SMOKERS; i++) {
                        takenResources.add(waitingSmokers.get((int)(Math.random() * waitingSmokers.size())).getResourceType());
                    }
                    table.putResourceToTheTable(takenResources);
                    takenResources.clear();
                } else {
                    System.out.println("Бармен не может взять ресурсы");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}