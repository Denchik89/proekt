package Page3;

/**
 * enum класс ResourceType
 * @author Denis
 * @version 1.0
 */
public enum ResourceType {
    MATCHES, TOBACCO, PAPER
}