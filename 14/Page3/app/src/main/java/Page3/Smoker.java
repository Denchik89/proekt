package Page3;

/**
 * Класс Smoker
 * использует интерфейс Runnable
 * @author Denis
 * @version 1.0
 */
public class Smoker implements Runnable {

    /** @value начало диапозон рандомного засыпания потока */
    private static final int LATENCY_START = 3000;

    /** @value конец диапозона рандомного засыпания потока */
    private static final int LATENCY_FINISH = 5000;

    /** @value начало диапозон рандомной задержки курильщика */
    private static final int LATENCY_SMOKING_START = 3000;

    /** @value конец диапозона рандомной задержки курильшика */
    private static final int LATENCY_SMOKING_FINISH = 5000;

    /** @value имя курильщика */
    private String name;

    /** @value экземпляр класса ResourceType */
    private ResourceType resourceType;

    /** @value стол за котором сидят курильщики */
    private Table table;

    /** @value статус курильщика */
    private SmokerStatus status = SmokerStatus.WAITING;

    /**
     * конструктор Smoker
     * @param name - имя курильщика
     * @param resourceType - ресурсы
     * @param table - стол за котором сидят курильщики
     */
    Smoker(String name, ResourceType resourceType, Table table) {
        this.name = name;
        this.resourceType = resourceType;
        this.table = table;
    }

    /**
     * метод getStatus
     * @return status - статус курильщика
     */
    public SmokerStatus getStatus() {
        return status;
    }

    /**
     * метод getResourceType
     * @return класс ResourceType
     */
    public synchronized ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * метод checkResourceTable
     * проверяет наличие ресурсов на столе
     * @return boolean
     */
    public boolean checkResourceTable() {
        switch (this.resourceType) {
            case PAPER:
                return table.hasMatches() && table.hasTobacco();

            case MATCHES:
                return table.hasPaper() && table.hasTobacco();
        }

        return table.hasMatches() && table.hasPaper();
    }

    /**
     * метод takeResourceFromTable
     * беред необходимы ресурсы на столе
     * @return класс ResourceType
     */
    public ResourceType[] takeResourceFromTable() {
        switch (this.resourceType) {
            case PAPER:
                return new ResourceType[]{table.getMatches(), table.getTobacco()};

            case MATCHES:
                return new ResourceType[]{table.getPaper(), table.getTobacco()};

            case TOBACCO:
                return new ResourceType[]{table.getMatches(), table.getPaper()};
        }
        return null;
    }

    /**
     * метод запуска потока
     */
    public void run() {
        while (true) {
            try {
                status = SmokerStatus.WAITING;
                Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
                if (checkResourceTable()) {
                    takeResourceFromTable();
                    status = SmokerStatus.SMOKING;
                    System.out.println(this.name + " ушел курить");
                    Thread.sleep(LATENCY_SMOKING_START + (int) (Math.random() * LATENCY_SMOKING_FINISH));
                    System.out.println(this.name + " вернулся");
                } else {
                    System.out.println(this.name + " не нашел нужных ресурсов");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}