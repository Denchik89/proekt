package Page1;


/**
 * Класс Reader
 * наследуется от java класса Thread
 * @author Denis
 * @version 1.0
 */
public class Reader extends Thread {

    /** @value экземпляр класса Book*/
    private Book book;

    /** @value число для нумерации потока читателя  */
    private int number;

    /** @value начало диапозон рандомного засыпания потока */
    static final int LATENCY_START = 4000;

    /** @value конец диапозона рандомного засыпания потока */
    static final int LATENCY_FINISH = 7000;

    /** конструктор Reader
     * @param book - экземпляр класса Book
     */
    Reader(Book book) {
        this.book = book;
        number = (int) (Math.random() * 8);
    }

    /**
     * метод который запускает поток
     */
    public void run() {
        String message = " ";
        while (message != ""){
            try {
                Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
                message = this.book.toRead(number);
                System.out.println("Я читатель № " + number + " прочитал " + message);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

