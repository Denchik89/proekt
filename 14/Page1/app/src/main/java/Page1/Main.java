package Page1;

public class Main {
    public static void main(String[] args) {
        Book book = new Book();
        Thread[] threads = {
                new Thread(new Writer(book)),
                new Thread(new Reader(book)),
                new Thread(new Reader(book)),
                new Thread(new Writer(book)),
                new Thread(new Reader(book)),
                        new Thread(new Reader(book))
        };

        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }
    }
}

