package Page1;

import java.util.concurrent.Semaphore;

/**
 * Класс Book
 * наследуется от java класса Thread
 * @author Denis
 * @version 1.0
 */
public class Book {

    private static final int LATENCY_READ_FINISH = 3000;
    private static final int LATENCY_READ_START = 2000;
    /** @value поле дата хранит в себе записанную строку */
    private String data;

    /** @value доступ к книге*/
    public Semaphore semaphore = new Semaphore(1, true);

    ReaderStatus status = ReaderStatus.NOTREAD;

    /**
     * метод возвращает записанную строку
     * @return - data
     */
    public String getData(){
        return this.data;
    }

    /** @value начало диапозон рандомного засыпания потока */
    static final int LATENCY_WRITE_START = 5000;

    /** @value конец диапозона рандомного засыпания потока */
    static final int LATENCY_WRITE_FINISH = 7000;

    /**
     * метод записывает в поле data строку
     * @param data - дата
     * @throws InterruptedException
     */
    public synchronized void write(String data, int number) throws InterruptedException {
        if (status.equals(ReaderStatus.NOTREAD)) {
            this.semaphore.acquire();
            System.out.println("Я писатель " + number + " начинаю писать");
            Thread.sleep(LATENCY_WRITE_START + (int) (Math.random() * LATENCY_WRITE_FINISH));
            System.out.println("Записал " + data);
            this.data = data;
            notifyAll();
            this.semaphore.release();
        } else {
            wait();
        }
    }

    /**
     * метод беред из поле данных строку
     * @throws InterruptedException
     * @return - data
     */
    public synchronized String toRead(int number) throws InterruptedException {
        if (this.data == null && this.semaphore.availablePermits() == 0){
            wait();
        } else if (this.semaphore.availablePermits() == 1){
            status = ReaderStatus.READ;
            System.out.println("Я читатель № " + number + " начинаю читать");
            Thread.sleep(LATENCY_READ_START + (int) (Math.random() * LATENCY_READ_FINISH));
            status = ReaderStatus.NOTREAD;
            notifyAll();
        }
        return this.data;
    }
}




