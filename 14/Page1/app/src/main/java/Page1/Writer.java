package Page1;


import java.util.Random;

/**
 * Класс Writer
 * наследуется от java класса Thread
 * @author Denis
 * @version 1.0
 */
public class Writer extends Thread {

    /** @value экземпляр класса Book*/
    private Book book;

    /** @value поле хранит длину генерируемой строки */
    public static final int SIZE_GENERATION_STRING = 15;

    /** @value число для нумерации потока читателя  */
    private int number;

    /** @value начало диапозон рандомного засыпания потока */
    static final int LATENCY_START = 4000;

    /** @value конец диапозона рандомного засыпания потока */
    static final int LATENCY_FINISH = 7000;

    /**
     * метод generationString
     * генерирует случайное строку в 15 символов
     * @return сгенерированную строку
     */
    private static String generationString() {
        String name = "";
        Random random = new Random();
        for(int i = 0; i < SIZE_GENERATION_STRING; i++) {
            char c = (char)(random.nextInt(26) + 'a');
            name += c;
        }
        return name;
    }


    /** конструктор Writer
     * @param book - экземпляр класса Book
     */
    Writer(Book book) {
        this.book = book;
        number = (int) (Math.random() * 8);
    }

    /**
     * метод который запускает поток
     */
    public void run() {
        while (true) {
            try {
                this.book.write(generationString(), number);
                Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
