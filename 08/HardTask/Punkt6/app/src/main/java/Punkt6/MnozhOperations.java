package Punkt6;

public class MnozhOperations {

  public static int[][] Umnosh(int[][] arr1, int[][] arr2) throws NullPointerException {
    if (arr1 == null || arr2 == null){
      throw new NullPointerException();
    }
    int[][] arr3 = new int[arr1.length][arr1.length];
    int suma = 0;

    for (int i = 0; i < arr1.length; i++) {
      for (int j = 0; j < arr2[i].length; j++) {
        for (int x = 0; x < arr2[j].length; x++) {
          suma += arr1[i][x] * arr2[x][j];
        }
        arr3[i][j] = suma;
      }
    }
    return arr3;

  }

  public static void vivodMatrix(int[][] arr, int size) throws NullPointerException {
    if (arr == null) {
      throw new NullPointerException();
    }
    for(int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        System.out.print(arr[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }



}
