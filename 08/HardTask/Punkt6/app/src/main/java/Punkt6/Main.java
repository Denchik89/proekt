package Punkt6;

import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Введите количество матриц: ");
    int kol = scanner.nextInt();
    System.out.print("Введите размер матриц(размеры вводятся одинаково): ");
    int size = scanner.nextInt();

    GenerateMatrix[] arr = new GenerateMatrix[kol];
    for (int i = 0; i < kol; i++) {
        arr[i] = new GenerateMatrix(size);
    }


    long start = System.currentTimeMillis();

    int[][] matrix2 = arr[0].getMatrix();
    for (int i = 0; i < kol; i++){
      matrix2 = MnozhOperations.Umnosh(matrix2, arr[i].getMatrix());
      MnozhOperations.vivodMatrix(matrix2, size);

      long finish = System.currentTimeMillis();
      long time = finish - start;
      System.out.println(time + " миллисекунд");
      System.out.println("----------------------------");
    }


  }
}
