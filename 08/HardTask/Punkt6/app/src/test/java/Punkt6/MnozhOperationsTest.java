package Punkt6;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class MnozhOperationsTest {
  private int[][] arr1;
  private int[][] arr2;

  @BeforeEach
  public void initilization() {
    arr1 = new int[][] {{2, 4, 1},
                        {6, 9, 4},
                        {1, 2, 3}};

    arr2 = new int[][] {{6, 7, 5},
                        {2, 2, 9},
                        {4, 2, 1}};
  }

  @Test void checkUmnozh() {
    int[][] arrTest = new int[][] {{24, 48, 95},
                                    {165, 233, 348},
                                    {370, 387, 413}};
    assertArrayEquals(arrTest, MnozhOperations.Umnosh(arr1, arr2), "good");
  }

}
