package Punkt4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner num = new Scanner(System.in);

        System.out.print("Введите порог: ");
        double porog = num.nextDouble();


        System.out.print("Введите коэфициент: ");
        double kof = num.nextDouble();


        System.out.print("Введите максимальное допустимое время выполнения работы ");
        int timeM = num.nextInt();

        ModelingProgres exp = new ModelingProgres();
        exp.progres(porog, kof, timeM);
    }
}
