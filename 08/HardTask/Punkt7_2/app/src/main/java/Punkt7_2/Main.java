package Punkt7_2;
import java.util.Scanner;
import Punkt7_2.Action.*;
import java.util.*;
import java.io.*;

public class Main{
  public static void main(String[] args)  throws Exception{
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    int userInput = 0; //по дефолту на старте
    Node start = new Node();    //инициализируем узлы дерева
    Node level1_1 = new Node();
    Node level1_2 = new Node();
    Node level1_2_1 = new Node();
    Node level1_2_2 = new Node();
    Node level2_2_1 = new Node();
    Node level2_2_2 = new Node();
    start.addChildren(level1_1);
    start.addChildren(level1_2);
    start.addAction(new Action1("Битва с роботом"));

    level1_1.addChildren(level1_2_1);
    level1_1.addChildren(level1_2_2);
    level1_1.addAction(new Action2("Математические задачки"));


    level1_2.addChildren(level2_2_1);
    level1_2.addChildren(level2_2_2);
    level1_2.addAction(new Action3("Выдача еды"));

    level1_2_1.addAction(new Action4("Угадайте в каком стаканчике шарик"));
    level1_2_2.addAction(new Action5("Проверка на готовность выживания на необитаемом острове"));
    level2_2_1.addAction(new Action6("узнайте свой возраст"));
    level2_2_2.addAction(new Action7("Умножение чисел"));


      while(true){
        start.menu();
        boolean flag = true;
        while(flag) {
          try {
          userInput = Integer.parseInt(scanner.readLine());
          flag = false;
          } catch (NumberFormatException | IOException e) {
              System.out.println("Введите число!");
            }
        }
        System.out.println();

        if (userInput == 0){
          if (start.parent == null){
            break;
          }
          else start = start.parent;
        }

        if(userInput > 0 && userInput < start.action.size() + start.children.size()) {
          start = start.children.get(userInput - start.children.size() + 1);
        } else if(userInput > start.children.size() && userInput <= start.action.size() + start.children.size()){
          start.action.get(0).act();
        }


      }
  }
}
