package Punkt7_2.Action;

import java.util.Scanner;
import java.util.Random;
import java.util.*;
import java.io.*;

public class Action5 extends Action{

  public Action5(String message){
    super(message);
  }

  public void act() throws Exception{
    System.out.println("Вы попали на необитаем остров. Выберите правильные предметы чтобы выжить: " + "\n" +
                       "1) Топор" + "\n" +
                       "2) Мяч" + "\n" +
                       "3) Зажигалка" + "\n" +
                       "4) Кепка" + "\n" +
                       "5) Пистолет" + "\n" +
                       "6) Удочка" + "\n");

   ArrayList<Integer> arr = userVibor();
   podvodItog(arr);
  }


  public ArrayList<Integer> userVibor() {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    boolean flag = true;
    int i = 0;
    int userInput = 0;


    ArrayList<Integer> arr = new ArrayList<Integer>();
    System.out.println("Введите числа предметов которые нужны");
    while(flag) {
      System.out.println("Выберите действие: " + "\n" +
                         "1) Ввести число" + "\n" +
                         "2) Закончить");
      try {
        userInput = Integer.parseInt(scanner.readLine());
        if (userInput == 1) {
          int vvodChisla = Integer.parseInt(scanner.readLine());
          arr.add(vvodChisla);
        } else if (userInput == 2) {
          System.out.println("Ваш выбор закончен");
          flag = false;

        }
      } catch (NumberFormatException | IOException e) {
        System.out.println("Вы ввели неправильно");
      }
    }
    return arr;
  }

  public void podvodItog(ArrayList<Integer> arr) {
    ArrayList<Integer> pravilArr = new ArrayList<Integer>();
    pravilArr.add(1);
    pravilArr.add(3);
    pravilArr.add(6);
    int result = 0;
    if (arr.size() == pravilArr.size()) {
      for (int i = 0 ; i < arr.size(); i++) {
        for (int j = 0; j < pravilArr.size(); j++) {
          if (arr.get(i) == pravilArr.get(j)) {
            result ++;
          }
        }
      }
      if (result == arr.size()) {
        System.out.println("Молодцы вы явно готовы там жить");
      } else {
        System.out.println("Вы не выживите на необитаемом острове");
      }
    } else {
      System.out.println("Вы указали не все правильные ответы");
    }
  }



}
