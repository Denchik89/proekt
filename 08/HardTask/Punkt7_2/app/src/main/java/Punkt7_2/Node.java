package Punkt7_2;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import Punkt7_2.Action.*;

public class Node {
  ArrayList<Node> children = new ArrayList<Node>();
  Node parent;
  ArrayList<Action> action = new ArrayList<Action>();

  public void addAction(Action action){
    this.action.add(action);
  }

  public void addChildren(Node nextLevel){
    this.children.add(nextLevel);
    nextLevel.parent = this;
  }

  public void menu(){
    System.out.println("Меню:");
    if (this.parent == null){
      System.out.println("0 Выйти");
    }
    else{
      System.out.println("0 Вернуться на уровень назад");
    }
    int num = 1;
    for (int i = 0; i < this.children.size(); i++) {
      System.out.println(num + " " + "Перейти на следующий уровень");
      num++;
    }
    for (int i = 0; i < this.action.size(); i++) {
      System.out.println(num + " " + this.action.get(i).getMessage());
      num++;
    }
    System.out.println();
  }
}
