package Punkt7_2.Action;

import java.util.Scanner;
import java.util.Random;
import java.util.*;
import java.io.*;

public class Action6 extends Action {

  public Action6(String message){
    super(message);
  }

  public void act() throws Exception{
    System.out.println("Введите год рождения и мы скажем сколько вам лет");
    int result = userInput();
    int dateYears = 2022;
    int itog = dateYears - result;
    System.out.println("Ваш возраст = " + itog);

  }


  public int userInput() {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    boolean flag = true;
    int userInput = 0;
    while(flag) {
      try {
        userInput = Integer.parseInt(scanner.readLine());
        if (userInput >= 1000 && userInput <= 2022) {
          flag = false;
        } else {
          System.out.println("Вы указываете неправильно дату, должно быть 4 значное число");
        }

      } catch(NumberFormatException | IOException e) {
          System.out.println("Вы ввели неправильно");
        }
    }
    return userInput;
  }

}
