package Punkt7_2.Action;

import java.util.Scanner;
import java.util.Random;
import java.util.*;
import java.io.*;

public class Action7 extends Action {

  public Action7(String message){
    super(message);
  }

  public void act() throws Exception{
    System.out.println("Введите два числа которые хотите умножить");
    userInput();


  }

  public void userInput() {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    boolean flag = true;
    int point = 0;
    int userInput1 = 0;
    int userInputNew = 0;
    int userInput2 = 0;
    while(point <= 2) {
      try {
        userInput1 = Integer.parseInt(scanner.readLine());
        if (point == 0) {
          userInputNew = userInput1;
        }
        if (point == 1) {
          userInput2 = userInput1;
        }
        if (point == 2) {
          System.out.println("Вы ввели число " + userInputNew + " и число " + userInput2 + "\n" +
                             "Результат = " + (userInputNew + userInput2));
        }
        point++;
      } catch(NumberFormatException | IOException e) {
          System.out.println("Вы ввели неправильно");
        }
    }
  }

}
