package Punkt7_2.Action;

import java.util.InputMismatchException;

import java.io.*;
import java.util.*;
import java.util.Scanner;

public class Action4 extends Action {

  public Action4(String message){
    super(message);
  }

  public void act() throws Exception {
    int randomaizer = 1;
    randomaizer += Math.random() * 3;
    System.out.println("выберите 1 стаканчик в котором может лежать шарик" + "\n" +
                       "1) 1 стаканчик" + "\n" +
                       "2) 2 стаканчик" + "\n" +
                       "3) 3 стаканчик" + "\n" +
                       "4) 4 стаканчик");

    int userOutput = operations();
    if (userOutput == randomaizer) {
      System.out.println("Вы выиграли");
    } else {
      System.out.println("Вы проиграли");
    }
    System.out.println("\n");
  }


  public int operations() {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    boolean flag = true;
    int userInput = 0;

    while(true) {
      try {
        userInput = Integer.parseInt(scanner.readLine());
        if (userInput <= 4 && userInput >= 1) {
          flag = false;
          return userInput;
        } else {
          System.out.println("Вы ввели не в предложенном диапозоне");
        }
      } catch(NumberFormatException | IOException e) {
          System.out.println("Вы ввели неправильно");
        }
      }
  }


}
