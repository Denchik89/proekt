package Punkt7_2.Action;

import java.util.*;
import java.util.Scanner;
import java.io.*;

public class Action3 extends Action{

  public Action3(String message){
    super(message);
  }

  public void act() throws Exception{
    System.out.println("введите количество людей и их имена, которым хотите дать еду:");
    String[] massivNew = Action3.operations();
    System.out.println("\n");
    for (int i = 0; i < massivNew.length - 1; i++) {
      System.out.println(massivNew[i] + " выдали еду");
    }
  }

  public static String[] operations() throws Exception {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    int kolichestvo = 0;
    boolean flag = true;
    while(flag) {
      try {
        kolichestvo = Integer.parseInt(scanner.readLine());
        flag = false;
      } catch(NumberFormatException | IOException e) {
          System.out.println("Введите именно число!!!");
        }
    }
    String[] massiv = new String[kolichestvo + 1];
    int i = 0;
    while(i < kolichestvo) {
      try {
        String name = scanner.readLine();
          if (name != null && name.length() != 0) {
            massiv[i] = name;
            i++;
          } else {
            System.out.println("Введите что нибудь");
          }
      } catch(NoSuchElementException exc) {
          System.out.println("Вы ввели неправильно");
        }
    }
    return massiv;
  }

}
