package Punkt7_2.Action;
import java.util.*;
import java.io.*;
import java.util.Scanner;

public class Action1 extends Action{

  public Action1(String message){
    super(message);
  }

  public double personHp = 100;
  public double robotHp = 100;


  public void act() throws Exception {
    String name = "ваши действия: " + "\n" +
                  "1) действие: атака(урон в диапозоне 10-30)" + "\n" +
                  "2) действие: защита(снижает полученый урон от робота в диапозоне 5-10)";
    System.out.println(name);


    boolean flagGame = true;
    while (flagGame) {
      double personAttack = 15;
      double robotAttack = 15;
      double personZashita = 20;
      personAttack += Math.random() * 10;
      robotAttack += Math.random() * 10;
      personZashita += Math.random() * 10;
      int personInput = 0;
        personInput = checkingInt("Введите номер действия:", personInput);
        if (personInput == 1) {
          System.out.println("-------------------------------------------------------" + "\n");
          this.robotHp = personAttack(robotHp, personAttack);
          System.out.println("Вы нанесли роботу " + personAttack + " урона" + "\n" +
                             "У робота осталось " + robotHp + "hp");

          System.out.println("-------------------------------------------------------" + "\n");
          this.personHp = robotAttack(personHp, robotAttack, robotHp);
          System.out.println("Робот нанес " + robotAttack + " урона" + "\n" +
                             "Осталось у вас " + personHp + "hp");

          System.out.println("-------------------------------------------------------" + "\n");

        } else if (personInput == 2) { //при защите проблема с кодом
            System.out.println("-------------------------------------------------------" + "\n");
            this.personHp = personZashita(personHp, robotAttack, personZashita);
            System.out.println("Вы защитились" + "\n" +
                               "Робот нанес вам " + (robotAttack - personZashita) + "\n" +
                               "у вас осталось " + personHp + "hp");

            System.out.println("-------------------------------------------------------" + "\n");
          }
        if (personHp <= 0) {
          System.out.println("Вы проиграли");
          flagGame = false;
          personHp = 100;
          robotHp = 100;
        } else if (robotHp <= 0) {
          System.out.println("Поздравляю вы победили");
          flagGame = false;
          personHp = 100;
          robotHp = 100;
        }
    }
  }

  private double robotAttack(double personHp, double robotAttack, double robotHp) {
    personHp = personHp - robotAttack;
    return personHp;
  }
  private double personAttack(double robotHp, double personAttack) {
    robotHp = robotHp - personAttack;
    return robotHp;
  }
  private double personZashita(double personHp, double robotAttack, double personZashita) {
    personHp = personHp - (robotAttack - personZashita);
    return personHp;
  }

  private int checkingInt(String personVibor, int personInput) throws Exception {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    String message = "Введите число 1 или 2: ";
    System.out.println(message);
    boolean flag = true;
    try {
      while (flag) {
          personInput = Integer.parseInt(scanner.readLine());
          if (personInput == 1) {
            personInput = 1;
            flag = false;
            return personInput;
          } else if (personInput == 2) {
            personInput = 2;
            flag = false;
            return personInput;
          } else {
            System.out.println("Введите 1 или 2");
          }
      }
    } catch(NumberFormatException | IOException e) {
        System.out.println("Введите именно число!!!");
      }


    return personInput;
  }


}
