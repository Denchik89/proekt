package Punkt7;
import java.util.*;

public class PracticaVvoda {
  public static void main(String[] args) throws Exception {
    vvod();

  }


  public static void vvod() {
    Scanner scanner = new Scanner(System.in);
    boolean shouldRead = true;
    String message = "Введите число от 1, до 4: ";

    while (shouldRead) {
      System.out.println(message);
      try {
        int num = scanner.nextInt();
        if (num <= 4 && num >= 1) {
          System.out.println("Выполняется операция меню");
          shouldRead = false;
        }
      } catch(NoSuchElementException exc) {
        System.out.println("Введите именно число!!!");
        scanner.next(); //тип просто запрашиваем у пользователя ввести что угодно
        shouldRead = true;
      }
    }
    scanner.close();
  }
}
