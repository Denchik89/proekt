/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Punkt3;

public class SortArray {
  public static void sort(double[] a) {
    int i = 0;
    int s = a.length;
    double q = 0;
    while (i != (s - 1)) {
      if  (a[i] > a[i + 1]) {

         q = a[i + 1];
         a[i + 1] = a[i];
         a[i] = q;
         i = 0;
      } else {
          i = i + 1;
        }
    }
    for (i = 0; i < s; i++) {
      System.out.print(a[i] + ", ");
    }
    System.out.println();
  }

  public static double[] createSortedArray(double[] arr) {
    double[] arr2 = arr.clone();
    //sort(arr2);
    int s = arr2.length;
    double q = 0;
    int i = 0;
    while (i != (s - 1)) {
      if  (arr2[i] > arr2[i + 1]) {

         q = arr2[i + 1];
         arr2[i + 1] = arr2[i];
         arr2[i] = q;
         i = 0;
      } else {
          i = i + 1;
        }
    }
    for (i = 0; i < 5; i++) {
      System.out.print(arr2[i] + ", ");
    }
    return arr2;
  }
}
