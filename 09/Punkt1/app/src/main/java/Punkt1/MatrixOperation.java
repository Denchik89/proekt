package Punkt1;

class MatrixOperation {
  public int[][] result;

  public int[][] matrixSum(int[][] arr1, int[][] arr2) throws NullPointerException, MatrixException{
    if (arr1 == null || arr2 == null){
       throw new NullPointerException();
    }
    if (arr1.length != arr2.length || arr1[0].length != arr2[0].length){
      throw new MatrixException("пустая матрица");
    }
    result = new int[arr1.length][arr1[0].length];
    for (int i = 0; i < arr1.length; i++){
      for (int j = 0; j < arr1[0].length; j++){
        result[i][j] = arr1[i][j] + arr2[i][j];
      }
    }
    return result;
  }

  public int[][] matrixMinus(int[][] arr1, int[][] arr2) throws NullPointerException, MatrixException{
    if (arr1 == null || arr2 == null){
       throw new NullPointerException();
    }
    if (arr1.length != arr2.length || arr1[0].length != arr2[0].length){
      throw new MatrixException("высота первой матрицы не совпадает со второй  + \n" +
      "или ширина первой не совпадает со второй");
    }
    result = new int[arr1.length][arr1[0].length];
    for (int i = 0; i < arr1.length; i++){
      for (int j = 0; j < arr1[0].length; j++){
        result[i][j] = arr1[i][j] - arr2[i][j];
      }
    }
    return result;
  }

  public int[][] matrixUmnozh(int[][] arr1, int[][] arr2)  throws NullPointerException, MatrixException{
    if (arr1 == null || arr2 == null){
       throw new NullPointerException();
    }
    if (arr1[0].length != arr2.length){
      throw new MatrixException("высота и ширина не совпадают");
    }
    result = new int[arr1.length][arr2[0].length];
    for (int i = 0; i < arr1.length; i++){
      for (int j = 0; j < arr2[0].length; j++){
        for (int x = 0; x < arr2.length; x++){
          result[i][j] += arr1[i][x] * arr2[x][j];
        }
      }
    }
    return result;
  }

  public void printMatrix(int[][] arr) throws NullPointerException{
    if (arr == null){
      throw new NullPointerException();
    }
    else{
      for (int i = 0; i < arr.length; i++){
        for (int j = 0; j < arr[0].length; j++){
          System.out.format("%d ", arr[i][j]);
        }
        System.out.println();
      }
    }
    System.out.println();
  }

}
