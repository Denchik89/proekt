package Punkt1;

class MatrixException extends Exception {
  public String text;

  public MatrixException(String text) {
    this.text = text;
  }

  public String getText() {
    return this.text;
  }
}
