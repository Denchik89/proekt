package Punkt2;

public class Vector {
  private double x1;
  private double y1;
  private double x2;
  private double y2;

  public Vector(double x1, double y1, double x2, double y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }


  public double getX1() {
    return this.x1;
  }
  public void setX1(double x1) {
    this.x1 = x1;
  }

  public double getY1() {
    return this.y1;
  }
  public void setY1(double y1) {
    this.y1 = y1;
  }

  public double getX2() {
    return this.x2;
  }
  public void setX2(double x2) {
    this.x2 = x2;
  }

  public double getY2() {
    return this.y2;
  }
  public void setY2(double y2) {
    this.y2 = y2;
  }

  public String toString(){
    return "tocka1 {x1= " + getX1() + ", y1= " + getY1() + "}" + "\n" +
           "tocka2 {x2= " + getX2() + ", y2= " + getY2() + "}";
  }

  public Vector sumVectors(Vector vector){
    Vector newVector = new Vector(
    this.getX1(),
    this.getY1(),
    this.getX2() + (vector.getX2() - vector.getX1()),
    this.getY2() + (vector.getY2() - vector.getY1()));
    return newVector;
  }

  public static Vector sumVectors(Vector vector1, Vector vector2){
    Vector newVector = new Vector(
    vector1.getX1(),
    vector1.getY1(),
    vector1.getX2() + (vector2.getX2() - vector2.getX1()),
    vector1.getY2() + (vector2.getY2() - vector2.getY1()));
    return newVector;
  }

  public static Vector minusVectors(Vector vector1, Vector vector2){
    Vector newVector = new Vector(
    vector1.getX1(),
    vector1.getY1(),
    vector1.getX2() - (vector2.getX2() - vector2.getX1()),
    vector1.getY2() - (vector2.getY2() - vector2.getY1()));
    return newVector;
  }

  public Vector minusVectors(Vector vector){
    Vector newVector = new Vector(
    this.getX1(),
    this.getY1(),
    this.getX2() - (vector.getX2() - vector.getX1()),
    this.getY2() - (vector.getY2() - vector.getY1()));
    return newVector;
  }

  public Vector umnozhVectorsScalar(double scalar){
    Vector newVector = new Vector(
    this.getX1(),
    this.getY1(),
    this.getX2()*scalar,
    this.getY2()*scalar);
    return newVector;
  }

  public Vector delenieVectorsScalar(double scalar){
    Vector newVector = new Vector(
    this.getX1(),
    this.getY1(),
    this.getX2()/scalar,
    this.getY2()/scalar);
    return newVector;
  }

}
