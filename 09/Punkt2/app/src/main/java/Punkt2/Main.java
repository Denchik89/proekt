package Punkt2;

public class Main {
  public static void main(String[] args) {
    Vector a = new Vector(12.5, 18, 34, 36);
    Vector a1 = new Vector(2, 4, 5, 7);
    Vector a2 = new Vector(1, 3, 4, 6);

    System.out.println(a1.sumVectors(a1));
    System.out.println("--------------------------");
    System.out.println(a1.sumVectors(a1, a2));
    System.out.println("\n");
    System.out.println(a1.minusVectors(a1));
    System.out.println("--------------------------");
    System.out.println(a1.minusVectors(a1, a2));
    System.out.println("\n");
    System.out.println(a1.umnozhVectorsScalar(3));
    System.out.println("\n");
    System.out.println(a1.delenieVectorsScalar(3));
  }
}
