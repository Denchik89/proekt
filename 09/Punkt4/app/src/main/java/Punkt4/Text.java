package Punkt4;

 public class Text implements Vivod {

   public String text;

   Text(String text){
     this.text = text;
   }

   public String output(){
     return this.text;
   }
 }
