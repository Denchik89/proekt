package Punkt4;

 public class Polygon implements Vivod {
   protected int kolStoron;
   protected int znachStoron;

   Polygon(int znachStoron, int kolStoron){
     this.znachStoron = znachStoron;
     this.kolStoron = kolStoron;
   }

   public int getPerimeter(){
     int perimeter = this.kolStoron * this.znachStoron;
     return perimeter;
   }

   public String toString(){
     return "Периметр=  " + this.getPerimeter();
   }

   public String name = "Я полигон";
   public String output(){
       return name;
   }

 }
