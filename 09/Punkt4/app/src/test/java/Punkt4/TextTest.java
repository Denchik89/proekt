package Punkt4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TextTest {
     @Test void chekText() {
         Text text1 = new Text("Я текст 1");
         String name = text1.output();
         assertEquals("Я текст 1", name);
     }
 }
