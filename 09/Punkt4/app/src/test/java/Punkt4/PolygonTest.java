package Punkt4;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class PolygonTest {
  private Polygon polygon;

  @BeforeEach
  public void initization(){
    polygon = new Polygon(20, 3);
  }

  @Test void chekPolygon(){
    assertEquals("Я полигон", polygon.output());
  }
}
