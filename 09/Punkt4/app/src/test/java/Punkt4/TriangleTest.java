package Punkt4;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class TriangleTest {
  private Triangle triangle;

  @BeforeEach
  public void initization(){
    triangle = new Triangle(4);
  }

  @Test void PerimeterTest(){
    assertEquals(12, triangle.getPerimeter());
  }

  @Test void PerimeterStrTest(){
    assertEquals("Я треугольник и мой периметр = 12", triangle.output());
  }
}
