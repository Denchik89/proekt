/*
* This Java source file was generated by the Gradle 'init' task.
*/
package Punkt3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class RabochilTest {
  private Rabochil rabochil1;


  @BeforeEach
  public void initilization(){
    rabochil1 = new Rabochil("Денис", "Рабочий", 5000000);
  }

  @Test void chekName() {
    assertEquals("Денис", rabochil1.getName(), "good");
  }

  @Test void chekProfesia() {
    assertEquals("Рабочий", rabochil1.getProfesia(), "good");
  }

  @Test void chekZarplata() {
    assertEquals(5000000, rabochil1.getZarplata(), "good");
  }

  @Test void chekHaracterMetods() {
    String text = "Я рабочий и это мой характерный метод";
    assertEquals(text, rabochil1.haracterMetodsRabochil(), "good");
  }

  @Test void chekToString() {
    String proverka = "Мое имя: " + "Денис" + "\n"
    + "Моя профессия: " + "Рабочий" + "\n"
    + "Моя зарплата: " + "5000000" + "\n"
    + "Я рабочий";
    assertEquals(proverka, rabochil1.toString(), "good");
  }


}
