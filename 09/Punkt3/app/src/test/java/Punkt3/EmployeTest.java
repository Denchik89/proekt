package Punkt3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class EmployeTest {
  private Employe employe1;
  private Employe employe2;


  @BeforeEach
  public void initilization() {
    employe1 = new Employe("Рома", "Кто то", 97000);
    employe2 = new Employe("Рома", "Кто то", 97000);
  }

  @Test void chekName() {
    assertEquals("Рома", employe1.getName(), "good");
  }

  @Test void chekProfesia() {
    assertEquals("Кто то", employe1.getProfesia(), "good");
  }

  @Test void chekZarplata() {
    assertEquals(97000, employe1.getZarplata(), "good");
  }

  @Test void chekToString() {
    String proverka = "Мое имя: " + "Рома" + "\n"
    + "Моя профессия: " + "Кто то" + "\n"
    + "Моя зарплата: " + "97000" + "\n";
    assertEquals(proverka, employe1.toString(), "good");
  }

  @Test void chekEqualsFalse() {
    Employe employe3 = new Employe("Гоша", "Кто то", 97000);
    assertEquals(false, employe1.equals(employe3), "good");
  }

  @Test void chekEqualsTrue() {
    assertEquals(true, employe1.equals(employe2), "good");
  }

  @Test void chekHashCode() {
    assertEquals(97148, employe1.hashCode(), "good");
  }

}
