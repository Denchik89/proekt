package Punkt3;


public class Employe {
  protected String name;
  protected String profesia;
  protected int zarplata;

  public Employe(String name, String profesia, int zarplata) {
    this.name = name;
    this.profesia = profesia;
    this.zarplata = zarplata;
  }

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public String getProfesia() {
    return profesia;
  }
  public void setProfesia(String profesia) {
    this.profesia = profesia;
  }

  public int getZarplata() {
    return zarplata;
  }
  public void setZarplata(int zarplata) {
    this.zarplata = zarplata;
  }


  public String toString() {
    return "Мое имя: " + this.name + "\n"
           + "Моя профессия: " + this.profesia + "\n"
           + "Моя зарплата: " + this.zarplata + "\n";
  }



  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public boolean equals(Object e){
    if (this == e) return true;
    if (e == null || getClass() != e.getClass()) {
      return false;
    }

    Employe e1 = (Employe) e; //привидение типов
    if (this.profesia != e1.profesia) {
      return false;
    }
    if (this.zarplata != e1.zarplata) {
      return false;
    }
    return this.name.equals(e1.name);
  }

  public int hashCode() {
    return this.name.length() * 34 + this.profesia.length() * 2 + this.zarplata;
  }


}
