package Punkt3;


public class Rabochil extends Employe {

  public Rabochil(String name, String profesia, int zarplata) {
    super(name, profesia, zarplata);
  }

  @Override
  public String toString(){
    return "Мое имя: " + getName() + "\n"
    + "Моя профессия: " + getProfesia() + "\n"
    + "Моя зарплата: " + getZarplata() + "\n"
    + "Я рабочий";
  }

  public String haracterMetodsRabochil(){
    String text = "Я рабочий и это мой характерный метод";
    return text;
  }

}
