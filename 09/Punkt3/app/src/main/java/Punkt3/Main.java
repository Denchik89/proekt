package Punkt3;

class Main{

  public static void main(String[] args) {
    Employe[] employe = new Employe[4];

    employe[0] = new Buhgalter("Аня", "Бухгалтер", 40000);
    employe[1] = new Enginer("Коля", "Инженер", 70000);
    employe[2] = new GlavBuhgalter("Денис", "Главный бухгалтер", 2700000);
    employe[3] = new Rabochil("Алла", "Рабочий", 20000);

    for (int i = 0; i < employe.length; i++){
      System.out.println(employe[i]);
      System.out.println("\n");
    }

    Buhgalter buhgalter = new Buhgalter("Аня", "Бухгалтер", 12234);
    System.out.println(buhgalter.haracterMetodsBuhgalter());
    GlavBuhgalter glavBuhgalter = new GlavBuhgalter("Иван", "Главный бухгалтер", 24234);
    System.out.println(glavBuhgalter.haracterMetodsGlavBuhgalter());
    Enginer enginer = new Enginer("Ваня", "Инженер", 3433);
    System.out.println(enginer.haracterMetodsEnginer());
    Rabochil rabochil = new Rabochil("Гоша", "Рабочий", 23895);
    System.out.println(rabochil.haracterMetodsRabochil());
  }

}
