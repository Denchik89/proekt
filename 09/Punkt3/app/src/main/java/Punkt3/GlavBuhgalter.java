package Punkt3;


public class GlavBuhgalter extends Buhgalter {

  public GlavBuhgalter(String name, String profesia, int zarplata) {
    super(name, profesia, zarplata);
  }

  @Override
  public String toString(){
    return "Мое имя: " + getName() + "\n"
    + "Моя профессия: " + getProfesia() + "\n"
    + "Моя зарплата: " + getZarplata() + "\n"
    + "Я главный бухгалтер";
  }

  public String haracterMetodsGlavBuhgalter(){ 
    String text = "Я главный бухгалтер и это мой характерный метод";
    return text;
  }
}
