package Punkt3;


public class Enginer extends Employe {
  public Enginer(String name, String profesia, int zarplata) {
    super(name, profesia, zarplata);
  }

  @Override
  public String toString(){
    return "Мое имя: " + getName() + "\n"
    + "Моя профессия: " + getProfesia() + "\n"
    + "Моя зарплата: " + getZarplata() + "\n"
    + "Я инженер";
  }

  public String haracterMetodsEnginer(){ 
    String text = "Я инженер и это мой характерный метод";
    return text;
  }

}
