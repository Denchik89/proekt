package Punkt3;


public class Buhgalter extends Employe {

  public Buhgalter(String name, String profesia, int zarplata) {
    super(name, profesia, zarplata);
  }

  @Override
  public String toString(){
    return "Мое имя: " + getName() + "\n"
    + "Моя профессия: " + getProfesia() + "\n"
    + "Моя зарплата: " + getZarplata() + "\n"
    + "Я бухгалтер";
  }

  public String haracterMetodsBuhgalter(){ 
    String text = "Я бухгалтер и это мой характерный метод";
    return text;
  }
}
