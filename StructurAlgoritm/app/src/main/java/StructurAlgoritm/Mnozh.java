package StructurAlgoritm;

import java.util.*;


public class Mnozh<T> implements Set {


  private T[] spisok;
  private int point;
  private int size;

  public Mnozh() {
    this.point = 0;
    this.size = 1;
    this.spisok = (T[]) new Object[size];
  }

  public void clear() {
    spisok = null;
    point = 0;
    size = 1;

  }

  public boolean retainAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }
  public boolean removeAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean addAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean containsAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean remove(Object o) { //Удаляет указанный элемент из этого набора, если он присутствует (необязательная операция).
        for (int i = 0; i < size(); i++) {
            if(spisok[i] == o) {
                for (int j = i; j < size() - 1; j++) {
                    spisok[j] = spisok[j + 1];
                }
                point--;
                return true;
            }
        }
        return false;
    }


  private void uvelich(int newSize) {
    size = newSize;
    spisok = Arrays.copyOf(spisok, newSize);
  }

  public boolean add(Object o) {
    for (int i = 0; i < spisok.length; i++) {
      if (o == spisok[i]) {
        return false;
      }
    }
    if ((point + 1) == size) {
    uvelich(size + 1);
    }
    spisok[point] = (T) o;
    point++;
    return true;
   }

   public Object[] toArray() {//Возвращает массив, содержащий все элементы этого набора.
     spisok = Arrays.copyOf(spisok, size);
     return spisok;
   }


   public Object[] toArray(Object[] a) {
     if (a.length >= spisok.length) {
       size = a.length;
       Object[] newArr = Arrays.copyOf(a, size);
       return newArr;
     }
     return spisok;
   }


   public Iterator iterator(){ //Возвращает итератор по элементам в этом наборе. Элементы возвращаются в произвольном порядке (если только этот набор не является экземпляром какого-либо класса, предоставляющего гарантию).
    Iterator iterator = new NewMnozh();
    return iterator;
  }


  private class NewMnozh implements MnozhIterator {
    private int nextIndex = 0;

    public boolean hasNext() { //Возвращает значение true, если итерация содержит больше элементов.
      if (nextIndex <= size - 1) {
        return true;
      } else {
        return false;
      }
    }

    public Object next() { //Возвращает следующий элемент в итерации.
        Object retValue = spisok[nextIndex];
        nextIndex += 1;
        return retValue;
    }
  }

  public boolean contains(Object o) { //Возвращает значение true, если этот набор содержит указанный элемент.
    for(int i = 0; i < spisok.length; i ++) {
      if (o == spisok[i]) {
        return true;
      }
    }
    return false;
  }

  public boolean isEmpty() {
    return point == 0;
  }

  public int size() {
    return size;
  }

  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (o == this) {
      return true;
    }

    Mnozh mnozh = (Mnozh) o;
    if (mnozh.size() != this.size()) {
      return false;
    }

    int n = 0;
    if (this.spisok.length == mnozh.spisok.length) {
      for (int i = 0; i < spisok.length; i++) {
        for (int j = 0; j < mnozh.spisok.length; j++) {
          if (this.spisok[i] == mnozh.spisok[j]) {
            n++;
          }
        }
      }
    } else {
      return false;
    }
    if (n == spisok.length) {
      return true;
    }
    return false;
  }


  public int hashCode() {
    Mnozh mnozh = new Mnozh();
    int result = 31;
    for (int i = 0; i < spisok.length; i++) {
      result = result * 17 + mnozh.size();
    }
    return result;
  }

}
