import pytest

# тест на int
def test_int():
    result = 0
    try:
        a = 1 / 0
        assert result == a
    except ArithmeticError:
        pass



test_x = [(10)]


@pytest.mark.parametrize('x', test_x)
def test_arithmetic(x):
    assert type(x) == int


# тест на list

def test_list():
    result = [3, 43, 6]
    try:
        result.remove(5)
    except ValueError:
        pass


testsList = [([1, 2, 3], [1, 2, 3, 1])]

@pytest.mark.parametrize('arr, x', testsList)
def test_list_parametrize(arr, x):
    arr.append(1)
    result = arr
    assert x == result



# тест на set

def test_set():
    list_set = {2, 3, 4, 1}
    try:
        list_set.remove(8)
    except KeyError:
        pass


array_set = [({1, 3, 2, 5}, set())]


@pytest.mark.parametrize('arr, x', array_set)
def test_set_parametrize(arr, x):
    arr.clear()
    result = arr
    assert x == result

