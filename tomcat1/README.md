# 1 просмотр данных

Создать приложение, которое позволит просматривать данные в таблице.

## Требования:

- структура проекта - отдельно файлы для работы с БД, отдельно файлы приложения
- файлы приложения разделены по слоям (MVC)
- JavaFX с использованием TableView
- Web-версия - только с использованием сервлетов и JSP.

### Инструкция по построению веб-приложения через командную строку без использования автоматизации и развертывание class-файлов

1) Скачать и установить Tomcat по [инструкции](https://www3.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html)
2) Перейти в папку /apache-tomcat-8.5.78/
3) скачать [jar файл](https://jdbc.postgresql.org/download/postgresql-42.3.3.jar) и поместить его в каталог /apache-tomcat-8.5.78/lib/
4) Запустить докер и Создать таблицу holder2 с полями id, username, passport
5) Создать папку classes по пути /apache-tomcat-8.5.78/webapp/tomcat1/WEB-INF/classes
6) В Папке classes создать java-файлы BDConnection.java, DataServlet.java, PersonDB
7) Выполнить javac -cp (полный путь)/apache-tomcat-8.5.78/lib/postgresql-42.3.3.jar BDConnection.java
8) Выполнить javac (полный путь) PersonDB.java
9) Выполнить javac -cp (полный путь)/apache-tomcat-8.5.78/lib/servlet-api.jar -sourcepath . DataServlet.java
10) Перейти в /apache-tomcat-8.5.78/bin/
11) прописать chmod a+x ./catalina.sh
12) Выполнить ./catalina.sh run
13) Перейти на [сайт](http://localhost:8080/tomcat1/data).....  "!!Порт может отличаться, учтите это"

### построение веб-приложения через командную строку без использования автоматизации в war-файл, построение jar-файла

1) Скачать и установить Tomcat по [инструкции](https://www3.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html)
2) Перейти в папку /apache-tomcat-8.5.78/
3) скачать [jar файл](https://jdbc.postgresql.org/download/postgresql-42.3.3.jar) и поместить его в каталог /apache-tomcat-8.5.78/lib/
4) Запустить докер и Создать таблицу holder2 с полями id, username, passport
5) Создать папку classes по пути /apache-tomcat-8.5.78/webapp/tomcat1/WEB-INF/classes
6) В Папке classes создать java-файлы BDConnection.java, DataServlet.java, PersonDB
7) Выполнить javac -cp (полный путь)/apache-tomcat-8.5.78/lib/postgresql-42.3.3.jar BDConnection.java
8) Выполнить javac (полный путь) PersonDB.java
9) Выполнить javac -cp (полный путь)/apache-tomcat-8.5.78/lib/servlet-api.jar -sourcepath . DataServlet.java
10) Перейти в папку /apache-tomcat-8.5.78/webapp/tomcat1/
11) Прописать jar -cvf <название war-файла>.war *
12) Перейти в /apache-tomcat-8.5.78/bin/
13) прописать chmod a+x ./catalina.sh
14) Выполнить ./catalina.sh run
15) Перейти на [сайт](http://localhost:8080/tomcat1/data).....  "!!Порт может отличаться, учтите это"
