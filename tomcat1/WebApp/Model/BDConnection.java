package WebApp.Model;

import java.sql.*;
import java.util.ArrayList;


public class BDConnection {

    /** @value поле обьекта Connection */
    private static Connection connection;


    /**
     * обобщенный интерфейс для лямбды
     */
    public interface classModel<E>{E newModel(ResultSet resultSet);}

    /**
     * статичный метод createConnection
     * создает подключение к базе данных
     * @return поле обьекта Connection
     */
    public static Connection createConnection() {
        String username = "postgres";
        String password = "mysecretpassword";
        String url = "jdbc:postgresql://127.0.0.1:5432/postgres"; //тут нельзя указывать имя и пароль надо отдельно
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * статичный метод getConnection
     * проверяет храниться ли в поле connection что либо
     * @return поле обьекта Connection
     */
    public static Connection getConnection() {
        if (connection == null){
            connection = createConnection();
        }
        return connection;
    }

    /**
     * статичный метод executeSelect
     * получает данные с бд и запись в поля класса Peoples
     * @param query - запрос
     * @param lambda - лямбда
     * @return массив экземпляра класса ArrayList
     */
    private ArrayList<T> executeSelect(String query, classModel<T> lambda) {
        ArrayList<T> model = new ArrayList<T>();


        try {
            Statement statement = getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                model.add(lambda.newModel(resultSet));
            }

            if (statement != null) { statement.close(); }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return model;
    }

    /**
     * статичный метод executeUpdate
     * устанавает запрос на обновление, удаление записи в базе данных
     * @param query - запрос
     * @return void
     */
    private static void executeUpdate(String query) {
        try {
            Statement statement = getConnection().createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * статичный метод executeInsert
     * устанавает запрос на добавление записей в таблицу
     * @param query - запрос
     * @return void
     */
    private static void executeInsert(String query) {
        try {
            Statement statement = getConnection().createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    

}