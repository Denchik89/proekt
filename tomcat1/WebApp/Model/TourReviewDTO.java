package WebApp.Model;


import java.sql.SQLException;

/**
 * Класс TourReviewDTO
 * @author Denis
 * @version 1.0
 */
public class TourReviewDTO {


    /** @value комментарий пользователя */
    public String comment;

    /** @value оценка пользователя */
    public int rating;


    /**
     * метод getComment
     * @return возвращает комментарий пользователя
     */
    public String getComment() {return comment;}

    /**
     * метод getRating
     * @return возвращает оценка пользователя
     */
    public int getRating() {return rating;}


    /** конструктор TourReviewDTO
     * @param comment - комментарий пользователя
     * @param rating - оценка пользователя
     */
    public TourReviewDTO(String comment, int rating) throws SQLException {
        this.comment = comment;
        this.rating = rating;
    }

}