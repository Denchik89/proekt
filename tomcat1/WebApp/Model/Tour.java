package WebApp.Model;

import java.sql.*;
import java.util.ArrayList;


/**
 * Класс Tour Используется для установление связи с БД
 * и хранение данных в классе
 * @author Denis
 * @version 1.0
 */
public class Tour {

    /** @value поле хранит имя тура */
    private String nametour;

    /** @value поле хранит имя города */
    private String namecity;

    /** @value поле хранит комментарий */
    private String comment;

    /** @value поле хранит рейтинг */
    private int rating;

    /** @value поле хранит цену */
    private int price;

    /** @value экземпляр класса BDConnection */
    private static BDConnection<Tour> bdConnection = new BDConnection<Tour>();

    /** лямбда функция для создание экземпляра Tour и запись данных с бд в поля экземпляра */
    private static BDConnection.classModel<Tour> classModel = (resultSet) -> {
        Tour tour = new Tour();
        try{
            tour.setNameTour(resultSet.getString("city_to_tour.nametour"));
            tour.setNameCity(resultSet.getString("city_to_tour.namecity"));
            tour.setComment(resultSet.getString("review.comment"));
            tour.setRating(resultSet.getInt("review.rating"));
            tour.setPrice(resultSet.getInt("tour.price"));
        }
        catch (SQLException e ) {System.out.println(e.getMessage());}
        return tour;
    };

    /**
     * метод setNameTour
     * @param nametour - имя тура
     * @return возвращает void
     */
    public void setNameTour(String nametour) {this.nametour = nametour;}

    /**
     * метод getNameTour
     * @return возвращает имя тура
     */
    public String getNameTour() {return nametour;}

    /**
     * метод setNameCity
     * @param namecity - имя города
     * @return возвращает void
     */
    public void setNameCity(String namecity) {this.namecity = namecity;}

    /**
     * метод getNameCity
     * @return возвращает возвращает имя города
     */
    public String getNameCity() {return namecity;}

    /**
     * метод setComment
     * @param comment - комментарий
     * @return возвращает void
     */
    public void setComment(String comment) {this.comment = comment;}

    /**
     * метод getComment
     * @return возвращает комментарий
     */
    public String getComment() {return comment;}

    /**
     * метод setRating
     * @param rating - рейтинг
     * @return возвращает void
     */
    public void setRating(int rating) {this.rating = rating;}

    /**
     * метод getRating
     * @return возвращает рейтинг
     */
    public int getRating() {return rating;}

    /**
     * метод setPrice
     * @param price - цена тура
     * @return возвращает void
     */
    public void setPrice(int price) {this.price = price;}

    /**
     * метод getPrice
     * @return возвращает цену тура
     */
    public int getPrice() {return price;}

    /**
     * статичный метод getAll
     * выдает отчет
     * @param nametour - название тура
     * @return массив классов Years
     */
    public static ArrayList<Tour> getByTour(String nametour) {
        return executeSelect("select city_to_tour.namecity, tour.price" + "\n" +
                "from tour" + "\n" +
                "join city_to_tour  on tour.name = city_to_tour.nametour" + "\n" +
                "join city on city.name = city_to_tour.namecity" + "\n" +
                "left join review on tour.name = review.tour" + "\n" +
                "where nametour = '" + nametour + "';"

        );
    }


    /** метод executeSelect
     *  получает список класса Tour
     * @param query - запрос
     * @return возвращает список класса Tour
     */
    private static ArrayList<Tour> executeSelect(String query){
        ArrayList<Tour> tours = bdConnection.executeSelect(query, classModel);
        return tours;
    }


}