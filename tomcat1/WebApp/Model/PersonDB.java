package WebApp.Model;

/**
 * Класс данных пользователей PersonDB
 * @author Denis
 * @version 1.0
 */
public class PersonDB {

    /** @value поле хранит id пользователя */
    private int id;

    /** @value поле хранит имя пользователя */
    private String username;

    /** @value поле хранит пароль пользователя */
    private String password;


    /** конструктор PersonDB */
    public PersonDB() {}

    /** конструктор PersonDB
     * @param username - имя пользователя
     * @param password - пароль пользователя
     */
    public PersonDB(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /** конструктор PersonDB
     * @param id - id пользователя
     * @param username - имя пользователя
     * @param password - пароль пользователя
     */
    public PersonDB( int id, String username, String password){
        this.id = id;
        this.username = username;
        this.password = password;
    }


    /**
     * метод getID
     * @return возвращает id пользователя
     */

    public int getId () {
        return id;
    }

    /**
     * метод setID
     * @param id - id пользователя
     * @return возвращает void
     */
    public void setId (int id){
        this.id = id;
    }

    /**
     * метод getUsername
     * @return возвращает имя пользователя
     */
    public String getUsername () {
        return username;
    }

    /**
     * метод setUsername
     * @param username - имя пользователя
     * @return возвращает void
     */
    public void setUsername (String username){
        this.username = username;
    }

    /**
     * метод getPassword
     * @return возвращает пароль пользователя
     */
    public String getPassword () {
        return password;
    }

    /**
     * метод setPassword
     * @param password - пароль пользователя
     * @return возвращает void
     */
    public void setPassword (String password){
        this.password = password;
    }

    /**
     * метод toString
     * @return возвращает строку данных пользователя
     */
    public String toString () {
        return getClass().getSimpleName() + "(id: " + id +
                ", username: " + username +
                ", password: " + password + ")";
    }

}
