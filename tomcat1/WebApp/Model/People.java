package WebApp.Model;

import java.util.ArrayList;


/**
 * Класс Peoples Используется для установление связи с БД
 * и хранение данных пользователей
 * @author Denis
 * @version 1.0
 */
public class People {

    /** @value поле хранит id пользователя */
    private int id;

    /** @value поле хранит имя пользователя */
    private String firstName;

    /** @value поле хранит Фамилия пользователя */
    private String secondName;

    /** @value экземпляр класса BDConnection */
    private static BDConnection<People> bdConnection = new BDConnection<>();

    /**
     * метод setID
     * @param id - id пользователя
     * @return возвращает void
     */
    public void setID(int id) {
        this.id = id;
    }

    /**
     * метод getId
     * @return возвращает id пользователя
     */
    public int getId() {
        return id;
    }

    /**
     * метод setFirstName
     * @param firstName - имя пользователя
     * @return возвращает void
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * метод getFirstName
     * @return возвращает имя пользователя
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * метод setSecondName
     * @param secondName - фамилия пользователя
     * @return возвращает void
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * метод getSecondName
     * @return возвращает фамилию пользователя
     */
    public String getSecondName() {
        return secondName;
    }


    /** лямда
     * создает и заполняет экземпляр класса People
     */
    private static BDConnection.classModel<People> classModel = (resultSet) -> {
        People people = new People();
        try{
            people.setID(resultSet.getInt("id"));
            people.setFirstName(resultSet.getString("firstName"));
            people.setSecondName(resultSet.getString("secondName"));
        }
        catch (SQLException e ) {System.out.println(e.getMessage());}
        return people;
    };


    /** метод executeSelect
     *  получает список класса People
     * @param query - запрос
     * @return возвращает список класса People
     */
    private static ArrayList<People> executeSelect(String query){
        ArrayList<People> peoples = bdConnection.executeSelect(query, classModel);
        return peoples;
    }


    /**
     * статичный метод getAll
     * выдает список всех записей Peoples
     * @return массив экземпляра класса ArrayList хранящий в себе классы Peoples
     */
    public static ArrayList<People> getAll() {
        return executeSelect("SELECT peoples2.id, peoples2.firstName, peoples2.secondName FROM peoples2 order by peoples2.id asc");
    }


    /**
     * статичный метод create
     * создание новой записи в базе данных
     * @param firstName - имя пользователя
     * @param secondName - фамилия пользователя
     * @return void
     */
    public static void create(String firstName, String secondName) {
        BDConnection.executeInsert("INSERT INTO peoples2 (firstName, secondName) VALUES('" + firstName + "', '" + secondName +"')");
    }


    /**
     * статичный метод update
     * обновление какой либо записи в базе данных по id пользователя
     * @param id - id пользователя
     * @param firstName - имя пользователя
     * @param secondName - фамилия пользователя
     * @return void
     */
    public static void update(int id, String firstName, String secondName){
        BDConnection.executeUpdate("UPDATE peoples2 SET peoples2.firstName = '" + firstName + "', peoples2.secondName = '" + secondName + "' WHERE peoples2.id =  '" + id + "'");
    }


    /**
     * статичный метод delete
     * удаление какой либо записи в базе данных по id пользователя
     * @param id - id пользователя
     * @return void
     */
    public static void delete(int id){
        BDConnection.executeUpdate("DELETE FROM peoples2 WHERE peoples2.id = '" + id +"'");
    }


}