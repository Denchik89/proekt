package WebApp.Model;


import java.sql.SQLException;

/**
 * Класс TourAllDTO
 * @author Denis
 * @version 1.0
 */
public class TourAllDTO {

    /** @value название тура */
    public String nametour;


    /**
     * метод getNameTour
     * @return возвращает название тура
     */
    public String getNameTour() {
        return nametour;
    }



    /** конструктор TourAllDTO
     * @param nametour - название тура
     */
    public TourAllDTO(String nametour) throws SQLException {
        this.nametour = nametour;
    }

}