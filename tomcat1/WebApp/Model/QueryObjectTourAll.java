package WebApp.Model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Класс QueryObjectTourAll
 * @author Denis
 * @version 1.0
 */
public class QueryObjectTourAll {

    /** @value экземпляр класса BDConnection */
    private static BDConnection<TourAllDTO> bdConnection = new BDConnection<TourAllDTO>();




    /** лямбда функция для создание экземпляра TourAllDTO и запись данных с бд в поля экземпляра */
    private static BDConnection.classModel<TourAllDTO> modelTourAll = (resultSet) -> {
        TourAllDTO TourAllDTO = null;
        try{
            TourAllDTO = new TourAllDTO(
                    resultSet.getString("name")
            );
        }catch (SQLException e ){ System.out.println(e.getMessage());}
        return TourAllDTO;
    };


    /** метод
     * создание запроса для получение массива всех туров из базы данных
     * @return возвращает массив класса TourAllDTO
     */
    public static ArrayList<TourAllDTO> getByTourAll(){
        return executeSelect("select name" + "\n" +
                "from tour" + ";");
    }


    /** метод executeSelect
     * получает список класса TourAllDTO
     * @param query - запрос
     * @return возвращает массив класса TourAllDTO
     */
    private static ArrayList<TourAllDTO> executeSelect(String query){
        ArrayList<TourAllDTO> tourList = bdConnection.executeSelect(query, modelTourAll);
        return tourList;
    }

}