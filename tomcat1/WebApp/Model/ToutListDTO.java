package WebApp.Model;


import java.sql.SQLException;

/**
 * Класс TourListDTO
 * @author Denis
 * @version 1.0
 */
public class TourListDTO {

    /** @value название тура */
    public String nametour;

    /** @value название города */
    public String namecity;

    /** @value год основания города */
    public String founded;

    /** @value комментарий пользователя */
    public String comment;

    /** @value оценка пользователя */
    public int rating;

    /** @value цена тура */
    public int price;


    /**
     * метод getNameTour
     * @return возвращает название тура
     */
    public String getNameTour() {
        return nametour;
    }

    /**
     * метод getNameCity
     * @return возвращает название города
     */
    public String getNameCity() {
        return namecity;
    }

    /**
     * метод getFounded
     * @return возвращает год основания города
     */
    public String getFounded() {
        return founded;
    }

    /**
     * метод getPrice
     * @return возвращает цену тура
     */
    public int getPrice() {
        return price;
    }


    /** конструктор TourListDTO
     * @param nametour - название тура
     * @param namecity - название города
     * @param founded - год основания города
     * @param price - цена тура
     */
    public TourListDTO(String nametour, String namecity, String founded, int price) throws SQLException {
        this.nametour = nametour;
        this.namecity = namecity;
        this.founded = founded;
        this.price = price;
    }

}