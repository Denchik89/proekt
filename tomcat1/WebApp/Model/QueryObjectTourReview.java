package WebApp.Model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Класс QueryObjectTourReview
 * @author Denis
 * @version 1.0
 */
public class QueryObjectTourReview {

    /** @value экземпляр класса BDConnection */
    private static BDConnection<TourReviewDTO> bdConnection = new BDConnection<TourReviewDTO>();


    /** лямбда функция для создание экземпляра TourReviewDTO и запись данных с бд в поля экземпляра */
    private static BDConnection.classModel<TourReviewDTO> modelTourReview = (resultSet) -> {
        TourReviewDTO TourReviewDTO = null;
        try{
            TourReviewDTO = new TourReviewDTO(
                    resultSet.getString("comment"),
                    resultSet.getInt("rating")
            );
        }catch (SQLException e ){ System.out.println(e.getMessage());}
        return TourReviewDTO;
    };


    /** метод
     * создание запроса для получение массива TourReviewDTO из базы данных по имени тура
     * @param nametour - название тура
     * @return возвращает массив класса TourReviewDTO
     */
    public static ArrayList<TourReviewDTO> getByTourReview(String nametour) {
        return executeSelect("select comment, rating" + "\n" +
                "from review" + "\n" +
                "join tour on name = tour" + "\n" +
                "where name = '" + nametour + "';");
    }



    /** метод executeSelect
     * получает список класса TourReviewDTO
     * @param query - запрос
     * @return возвращает массив класса TourReviewDTO
     */
    private static ArrayList<TourReviewDTO> executeSelect(String query){
        ArrayList<TourReviewDTO> tourList = bdConnection.executeSelect(query, modelTourReview);
        return tourList;
    }

}