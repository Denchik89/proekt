package WebApp.Model;


import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Класс QueryObjectTourList
 * @author Denis
 * @version 1.0
 */
public class QueryObjectTourList {

    /** @value экземпляр класса BDConnection */
    private static BDConnection<TourListDTO> bdConnection = new BDConnection<TourListDTO>();


    /** лямбда функция для создание экземпляра TourListDTO и запись данных с бд в поля экземпляра */
    private static BDConnection.classModel<TourListDTO> modelTourList = (resultSet) -> {
        TourListDTO tourListDTO = null;
        try{
            tourListDTO = new TourListDTO(
                    resultSet.getString("nametour"),
                    resultSet.getString("namecity"),
                    resultSet.getString("founded"),
                    resultSet.getInt("price")
            );
        }catch (SQLException e ){ System.out.println(e.getMessage());}
        return tourListDTO;
    };


    /** метод
     * создание запроса для получение массива TourListDTO из базы данных по имени тура
     * @param nametour - название тура
     * @return возвращает массив класса TourListDTO
     */
    public static ArrayList<TourListDTO> getByTourList(String nametour){
        return executeSelect("select nametour, namecity, founded, price" + "\n" +
                "from tour" + "\n" +
                "join city_to_tour on tour.name = city_to_tour.nametour" + "\n" +
                "join city on city.name = namecity" + "\n" +
                "left join review on tour.name = review.tour" + "\n" +
                "where city_to_tour.nametour = '" + nametour + "';");
    }



    /** метод executeSelect
     * получает список класса TourListDTO
     * @param query - запрос
     * @return возвращает массив класса TourListDTO
     */
    private static ArrayList<TourListDTO> executeSelect(String query){
        ArrayList<TourListDTO> tourList = bdConnection.executeSelect(query, modelTourList);
        return tourList;
    }

}
