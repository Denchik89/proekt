<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="WebApp.Model.People" %>
<%@ page import="java.util.ArrayList" %>

<body>
    <h3>Список Пользователей: </h3>
        <p>
            <%for (People peoples : (ArrayList<People>) request.getAttribute("PeopleGetAll")) {%>
                <%= peoples.getId() %>
                <%= peoples.getFirstName() %>
                <%= peoples.getSecondName() %>
                <hr>
            <%}%>
        </p>
</body>

<p><a href = 'http://localhost:9999/tomcat1/mainsite'> Вернуться назад </a></p>