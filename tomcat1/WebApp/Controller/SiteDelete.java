package WebApp.Controller;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Класс SiteDelete
 * адрес "/mainsite/create"
 * @author Denis
 * @version 1.0
 */
@WebServlet("/mainsite/delete")
public class SiteDelete extends HttpServlet {


    /**
     * метод обработки GET-запросов
     * предоставляет пользователю удаление записи в базе данных по введеному id пользователя
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/site_delete.jsp").forward(request, response);

    }

    /**
     * метод обработки POST-запросов
     * предоставляет пользователю ссылки на удаление еще одной записи либо переход на главную страницу
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/delete_people.jsp").forward(request, response);

    }

}