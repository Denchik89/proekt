package WebApp.Controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;


/**
 * Класс ReportTour
 * адрес "/mainsite/report"
 * @author Denis
 * @version 1.0
 */
@WebServlet("/mainsite/report")
public class ReportTour extends HttpServlet {

    /**
     * метод обработки GET-запросов
     * предоставляет пользователю название тура по которому хочет посмотреть отчет
     * и кнопка- показать отчет
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        request.setAttribute("TourAll", WebApp.Model.QueryObjectTourAll.getByTourAll());
        request.setAttribute("TourList", WebApp.Model.QueryObjectTourList.getByTourList(request.getParameter("nametour")));
        request.setAttribute("Review", WebApp.Model.QueryObjectTourReview.getByTourReview(request.getParameter("nametour")));
        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/report_tour.jsp").forward(request, response);
    }



}
