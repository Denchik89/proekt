package WebApp.Controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;


/**
 * Класс сервлет DataServlet
 * адрес "/data"
 * @author Denis
 * @version 1.0
 */
@WebServlet("/data")
public class DataServlet extends HttpServlet {


    /**
     * метод обработки GET-запросов, вывод всех Holder
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        response.setContentType("text/html; charset=UTF-8");

        PrintWriter out = response.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head><title>My BD</title></head>");
        out.println("<body>");
        out.println("<h1>База данных</h1>");

        BDConnection connection = new BDConnection();
        String query = "select id, username, passport from holder2";

        try {
            Statement statement = connection.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            PersonDB person = new PersonDB();

            while (resultSet.next()){
                person.setId(resultSet.getInt("id"));
                person.setUsername(resultSet.getString("username"));
                person.setPassword(resultSet.getString("passport"));
                out.println("<p>" + person + "</p>");
            }

            if (statement != null) { statement.close(); }
        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        }

        out.println("</body></html>");
        out.close();
    }


}




