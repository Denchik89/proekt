package WebApp.Controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;


/**
 * Класс MainSite
 * адрес "/mainsite"
 * @author Denis
 * @version 1.0
 */
@WebServlet("/mainsite")
public class MainSite extends HttpServlet {

    /**
     * метод обработки GET-запросов
     * предоставляет пользователю ссылки на страницы: создание, обновление, удаление записей
     * и кнопку- показать все записи
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/mainsite.jsp").forward(request, response);
    }


    /**
     * метод обработки POST-запросов
     * показывать пользоваетлю все записи которые есть в бд
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        request.setAttribute("PeopleGetAll", WebApp.Model.People.getAll());

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/people_getAll.jsp").forward(request, response);

    }

}