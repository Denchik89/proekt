package WebApp.Controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;

/**
 * Класс SiteCreate
 * адрес "/mainsite/create"
 * @author Denis
 * @version 1.0
 */
@WebServlet("/mainsite/create")
public class SiteCreate extends HttpServlet {


    /**
     * метод обработки GET-запросов
     * предоставляет пользователю поля для ввода имени и фамилии
     * и кнопку- отправить введенные данные в базу данных
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/site_create.jsp").forward(request, response);

    }

    /**
     * метод обработки POST-запросов
     * предоставляет пользователю ссылки на создание еще одной записи либо переход на главную страницу
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/create_people.jsp").forward(request, response);

    }

}