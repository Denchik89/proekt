package WebApp.Controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;


/**
 * Класс SiteUpdate
 * адрес "/mainsite/update"
 * @author Denis
 * @version 1.0
 */
@WebServlet("/mainsite/update")
public class SiteUpdate extends HttpServlet {

    /**
     * метод обработки GET-запросов
     * предоставляет пользователю обновление записи в базе данных
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        request.getRequestDispatcher("/WEB-INF/classes/WebApp/View/site_update.jsp").forward(request, response);

    }

    /**
     * метод обработки POST-запросов
     * предоставляет пользователю ссылки на обновление еще одной записи либо переход на главную страницу
     * @throws IOException
     * @throws ServletException
     * @param request - объект HttpServletRequest содержащий запрос, поступивший от клиента
     * @param response - объект HttpServletResponse определяющий ответ  клиенту
     * @return возвращает void
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("firstName");
        String surname = request.getParameter("secondName");
        WebApp.Model.People.update(id, name, surname);

        PrintWriter out = response.getWriter();

        out.println("<p><a href = 'http://localhost:9999/tomcat1/mainsite/update'> Обновить еще одну запись </a></p>");
        out.println("<p><a href = 'http://localhost:9999/tomcat1/mainsite'> Вернуться на главную страницу </a></p>");

    }

}