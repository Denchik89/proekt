package Page1;

import java.util.Random;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.ArrayList;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;


/**
 * Класс Operations
 * @author Denis
 * @version 1.0
 */
public class Operations {

    /** @value Количество пар ключ-значений Map */
    public static final int MAXVALUE = 100;

    /** @value длина генерируемой строки */
    public static final int SIZE_GENERATION_STRING = 10;

    /** @value максимальный возраст */
    public static final int MAXAGE = 30;


    /** статичный метод generationRandomString
     * @return сгенерированную строку
     */
    private static String generationRandomString() {
        String name = "";
        Random r = new Random();
        for(int i = 0; i < SIZE_GENERATION_STRING; i++) {
            char c = (char)(r.nextInt(26) + 'a');
            name += c;
        }
        return name;

    }

    /** статичный метод generationMap
     * генерирует Map и заполняет его
     * @return экземпляр класса Map
     */
    private static Map<Integer, Person> generateMap() {
        Map<Integer, Person> map = new HashMap<Integer, Person>();
        for (int i = 0; i < MAXVALUE; i++) {
            map.put(i, new Person(generationRandomString(), generationRandomString(), (int) (Math.random() * MAXAGE)));
        }
        return map;
    }

    /** статичный метод saveFile
     * разлагает в байты и записывать в файл
     * @param fileName - путь и название файла
     * @return void
     */
    public static void saveFile(String fileName) {

        FileOutputStream fileOutputStream = null;
        ObjectOutputStream out = null;
        
        try {
            fileOutputStream = new FileOutputStream(fileName);
            out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(generateMap());
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    /** статичный метод lookFile
     * превращает байты в обьект
     * @param fileName - путь и название файла
     * @return void
     */
    public static void lookFile(String fileName) {

        Map<Integer, Person> map = new HashMap<Integer, Person>();

        FileInputStream fileInputStream = null;
        ObjectInputStream in = null;
        
        try {
            fileInputStream = new FileInputStream(fileName);
            in = new ObjectInputStream(fileInputStream);
            map = (Map<Integer, Person>)in.readObject();
            in.close();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch(ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        Set<Map.Entry<Integer, Person>> set = map.entrySet();
        for (Map.Entry<Integer, Person> me : set) {
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
            System.out.println("\n");
        }
    }

}