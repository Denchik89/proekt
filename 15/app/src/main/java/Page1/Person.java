package Page1;

import java.io.Serializable;

/**
 * Класс Person
 * использует интерфейс Serializable
 * @author Denis
 * @version 1.0
 */
public class Person implements Serializable {

    /** @value имя Персонажа */
    private String firstName;

    /** @value фамилия Персонажа */
    private String secondName;

    /** @value фозраст Персонажа */
    private int age;

    /** @value поле контроля версии */
    private static final long serialVersionUID = -3722203743604454371L;

    /**
     * метод getFirstName
     * @return возвращает имя Персонажа
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * метод getSecondName
     * @return возвращает фамилию Персонажа
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * метод getAge
     * @return возвращает возраст Персонажа
     */
    public int getAge() {
        return age;
    }

    /** конструктор Person
     * @param firstName - имя Персонажа
     * @param secondName - фамилия Персонажа
     * @param age - фозраст Персонажа
     */
    public Person(String firstName, String secondName, int age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
    }

    /** переопределенный метод toString
     * @return строку
     */
    public String toString() {
        return "Name: " + getFirstName() + "\n" +
                "SecondName: " + getSecondName() + "\n" +
                "Age: " + getAge();
    }
}