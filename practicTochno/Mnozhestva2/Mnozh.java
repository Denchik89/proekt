package Mnozhestva2;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;


public class Mnozh<T> implements Collection {

  private HashSet<String> spisok;
  private Mnozh[] spisok2;

  private int kol = 0;


  public Mnozh(HashSet<String> spisok) {
    this.spisok = spisok;
  }


  public void clear() {
    this.spisok = null;
  }

  public boolean retainAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }
  public boolean removeAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean addAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean containsAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean remove(Object element) {
    return true;
  }

  public boolean add(Object arr) {
    if (arr == spisok) {
      return true;

    }
    this.kol++;
    return false;
  }

  public Mnozh[] toArray(){
   return spisok2;
  }

  public Mnozh[] toArray(Object[] c){
   return spisok2;
  }

  public boolean contains(Object o) {
    if (o == this.spisok) {
      return true;
    }
    return false;
  }

  public boolean isEmpty(){
    return (this.spisok == null);
  }

  public int size() {
    if (kol != 0) {
      return this.kol;
    }
    return 0;
  }


  interface MnozhIterator extends java.util.Iterator { }

  private class Mnozh2Iterator implements MnozhIterator{


    public boolean hasNext(){
      return spisok != null;
    }

    public HashSet next(){
      HashSet<String> result = spisok;
      return result;
    }
  }


  public MnozhIterator iterator(){
    MnozhIterator iterator = this.new Mnozh2Iterator();
    return iterator;
  }

}
